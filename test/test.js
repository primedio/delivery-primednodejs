'use strict';
require('dotenv').config()

var expect = require('chai').expect;
var axios = require('axios');
var dateFormat = require('dateformat');
var PRIMED = require('../primedio');

var primedio_url = process.env.PRIMEDIO_URL;
var public_key = process.env.PRIMEDIO_APIKEY_PUBKEY;
var secret_key = process.env.PRIMEDIO_APIKEY_SECRETKEY;
var user = process.env.PRIMEDIO_AUTH_USERNAME;
var password = process.env.PRIMEDIO_AUTH_PASSWORD;

describe('initialization', function() {
    it('_dt_format() should return a timestamp', function() {
        var pio = new PRIMED({   
            api_url: "https://test.primed.io/api/v1",
            public_key: "public_key", 
            secret_key: "public_key"
        });
        var dt = new Date('2014-04-03');

        var expected = "1396483200";
        var actual = pio._dt_format(dt);

        expect(actual).to.equal(expected);
    })

    it('signature should be valid', function() {
        var pio = new PRIMED({   
            api_url: "https://test.primed.io/api/v1",
            public_key: "public_key", 
            secret_key: "public_key"
        });

        var nonce = pio._dt_format(new Date('2014-04-03'));
        var public_key = "this_is_fake";
        var private_key = "this_is_also_fake";
        
        var expected = "71c500ca5a6b22c805da2885c4b9ea795b0a6b57369917b9c7814f6d679aeb40c1cca6d965f9a0cf621eb0d02997ea3c5bb5e908c61b257e60f0442bf37eb3e5";
        var actual = pio._signature(nonce, public_key, private_key);
        
        expect(actual).to.equal(expected);
    })
});

describe('it', function() {
    before(function(done){
        axios.post(primedio_url +'/api/v1/testhelper/setup', {}, {
            auth: {
                username: user,
                password: password
            }
        })
        .then(function (response) {
            done(); 
        })
        .catch(function (error) {
            console.log(error);
            done(error);
        });
    })

    after(function(done){
        axios.post(primedio_url +'/api/v1/testhelper/teardown', {}, {
            auth: {
                username: user,
                password: password
            }
        })
        .then(function (response) {
            done(); 
        })
        .catch(function (error) {
            console.log(error);
            done(error);
        });
    })

    describe('should perform personalise calls', function() {
        it('should return results for __CONTROL__', function(done) { 
            var pio = new PRIMED({
                api_url: primedio_url,
                public_key: public_key, 
                secret_key: secret_key
            });

            // Perform the personalise call,
            // which returns a 'promise'
            var promise = pio.personalise({
                campaign: 'mycampaign',
                limit: 3,
                abvariant: '__CONTROL__'
            });

            // Deal with the results as they are returned
            // by Primed. 
            promise.then(function(response) {               
                expect(response.all()).to.have.lengthOf(3);
                done();
            }).catch(function (err) {
                console.log('Personalize failed: ' + err);
                done(err);
            });
        });

        it('should return blended results for A', function(done) {
            var pio = new PRIMED({
                api_url: primedio_url,
                public_key: public_key, 
                secret_key: secret_key
            });

            // Perform the personalise call,
            // which returns a 'promise'
            var promise = pio.personalise({
                campaign: 'mycampaign',
                signals: {device: "A"},
                limit: 3
            });

            // Deal with the results as they are returned
            // by Primed. 
            promise.then(function(response) {
                expect(response.all()).to.have.lengthOf(3);
                done();
            }).catch(function (err) {
                console.log('Personalize failed: ' + err);
                done(err);
            });
        });
    });

    describe('should perform conversion calls', function() {
        it('should accept conversion without payload', function(done) {
            var pio = new PRIMED({
                api_url: primedio_url,
                public_key: public_key, 
                secret_key: secret_key
            });

            // Perform the personalise call,
            // which returns a 'promise'
            var promise = pio.convert({
                ruuid: 'someruuid'
            });

            // Deal with the results as they are returned
            // by Primed. 
            promise.then(function(response) {
                done();
            }).catch(function (err) {
                console.log('Convert failed: ' + err);
                done(err);
            });
        });

        it('should accept conversion with payload', function(done) {
            var pio = new PRIMED({
                api_url: primedio_url,
                public_key: public_key, 
                secret_key: secret_key
            });

            // Perform the personalise call,
            // which returns a 'promise'
            var promise = pio.convert({
                ruuid: 'someruuid',
                data: {'heartbeat': 10}
            });

            // Deal with the results as they are returned
            // by Primed. 
            promise.then(function(response) {
                done();
            }).catch(function (err) {
                console.log('Convert failed: ' + err);
                done(err);
            });
        });
    });
});