//     primedio.js 2.1.0
//     https://www.primed.io
//     (c) 2017 Matthijs van der Kroon, PrimedIO
//     PrimedIO-NodeJS may be freely distributed under the MIT license.

'use strict';

// PRIVATE
var sha512 = require('js-sha512').sha512;
var dateFormat = require('dateformat');
var options = {
    mimetypes: {
            json: ["application/json", "application/json;charset=utf-8"]
            
        }
    };
var Client = require('node-rest-client').Client;
var restclient = new Client(options);
var Result = require('./result').Result;
var ResultSet = require('./result').ResultSet;

// logging
var debug = require('debug')
var log = debug('primedio')


// PUBLIC
module.exports = PRIMED;

/**
 * Instantiate PrimedIO API wrapper
 * @param {string} api_url api_url key as provided by PrimedIO
 * @param {string} public_key public key as provided by PrimedIO
 * @param {string} secret_key secret key as provided by PrimedIO
 */
function PRIMED({
    api_url,
    public_key,
    secret_key
}) {

    var self = this;
    self.api_url = api_url;
    self.public_key = public_key;
    self.secret_key = secret_key;
};

/**
 * Get personalization from PrimedIO
 * @param {string} campaign campaign key
 * @param {Object} signals signals for which to match models
 * @param {number} limit number of desired personalization results, defaults to 5
 * @param {string} abvariant abvariant to be used for this personalise call
 */
PRIMED.prototype.personalise = function({
    campaign, 
    signals = {}, 
    limit = 5, 
    abvariant
}) {
    if (!(signals instanceof Object)) throw new Error('`signals` should be an object, you provided: ' + (typeof signals));
    if (typeof campaign !== 'string') throw new Error('`campaign` should be a String, you provided: ' + (typeof campaign));
    if ((abvariant !== undefined) && (typeof abvariant !== 'string')) throw new Error('`abvariant` should be a String, you provided: ' + (typeof abvariant));

    var self = this;

    var now = new Date();
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var timebins = ["00-03", "03-06", "06-09", "09-12", "12-15", "15-18", "18-21", "21-24"];

    var current_toddow_bin = days[now.getDay()] + "_" + timebins[Math.floor(now.getHours()/3)]

    var url = self.api_url + '/api/v1/personalise';

    return new Promise(
        function (resolve, reject) {
            
            var nonce = self._dt_format(new Date());
            var signature = self._signature(nonce, self.public_key, self.secret_key);

            // set content-type header and data as json in args parameter 
            var args = {
                parameters: {
                    "signals": JSON.stringify(Object.assign({toddow: current_toddow_bin, SYSTEM_: '*'}, signals)),
                    "limit": limit,
                    "campaign": campaign
                },
                headers: { 
                    "Content-Type": "application/json",
                    "X-Authorization-Key": self.public_key,
                    "X-Authorization-Signature": signature,
                    "X-Authorization-Nonce": nonce
                }
            };

            if (abvariant !== undefined) {
                args['parameters']['abvariant'] = abvariant;
            }
            
            log({
                "campaign": campaign,
                "primedio_url": url,
                "parameters": args["parameters"]
            });

            var req = restclient.get(url, args, function (data, response) {
                if (response.statusCode != 200) {
                    reject("request failed: " + response.statusCode);
                } else {

                    // parse results dictionaries as Result objects
                    var results = [];    
                    for (var i = 0, len = data["results"].length; i < len; i++) {
                        var result = new Result(
                            data["results"][i]["components"], 
                            data["results"][i]["recency_factor"],
                            data["results"][i]["ruuid"],
                            data["results"][i]["target"],
                            data["results"][i]["fscore"]
                        );
                        results.push(result);
                    }

                    var result_set = new ResultSet(
                        data["experiment"]["abvariant"]["label"],
                        data["experiment"]["abvariant"]["dithering"],
                        data["experiment"]["abvariant"]["recency"],
                        data["guuid"],
                        data["query_latency_ms"],
                        results
                    );

                    resolve(result_set);
                }
            });

            // handle errors
            req.on('error', function (err) {
                reject(err);
            });

        }
    );

};

/**
 * Register a result as having converted using the Result UUID
 * @param {string} ruuid ruuid for which to register conversion
 * @param {Object} data optional data field 
 */
PRIMED.prototype.convert = function({
    ruuid, 
    data
}) {
    var self = this;

    if (typeof ruuid !== 'string') throw new Error('`ruuid` should be a String, you provided: ' + (typeof ruuid));
    if ((data !== undefined) && !(data instanceof Object))throw new Error('`data` should be an object, you provided: ' + data);
    
    var url = self.api_url + "/api/v1/conversion/" + ruuid;
    
    log(url);

    return new Promise(
        function (resolve, reject) {
            // set content-type header and data as json in args parameter
            var nonce = self._dt_format(new Date());
            var signature = self._signature(nonce, self.public_key, self.secret_key);

            // set content-type header and data as json in args parameter 
            var args = {
                headers: { 
                    "Content-Type": "application/json",
                    "X-Authorization-Key": self.public_key,
                    "X-Authorization-Signature": signature,
                    "X-Authorization-Nonce": nonce
                }
            };

            if (data !== undefined) {
                args['data'] = data
            }
             
            var req = restclient.post(url, args, function (data, response) {
                 if (response.statusCode != 200) {
                    reject("request failed: " + response.statusCode);
                } else {
                    resolve(response);
                }
                
            });

            // it's useful to handle request errors to avoid, for example, socket hang up errors on request timeouts 
            req.on('error', function (err) {
                console.error('request error', err);
                reject(err);
            });

        }
    );
    
}

PRIMED.prototype._signature = function(nonce, public_key, secret_key) {
    return sha512(public_key + "" + secret_key + "" + nonce);
}

PRIMED.prototype._dt_format = function(dt) {
    return Math.floor(new Date(dt).getTime()/1000) + "";
}