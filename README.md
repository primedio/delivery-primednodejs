# primednodejs

Create a personalized web application that is unique and relevant for each and every user with [Primed.io](https://www.primed.io/).

# Use

## Installation
```shell
npm install primednodejs
```

## Example
```javascript
var PRIMEDIO = require('primednodejs');

// Instantiate PRIMEDIO, api_url, public key
// and secret key are provided by PrimedIO
var pio = new PRIMEDIO({
	api_url: "https://primed.io",
	public_key: 'xxxxxxxxxxxxxxxxxxxxxxxx', 
	secret_key: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
});

// Perform the personalise call,
// which returns a 'promise'
var promise = pio.personalise({
	campaign: 'frontpage.article.bottom',
	signals: { 
		'article_id': '2578346', 
		'tod-dow': 'Fri_15-18'
	},
	limit_results: 10
});

// Deal with the results as they are returned
// by Primed. 
promise.then(function(response) {
	// 'response' is a wrapper object,
	// calling 'response.all()' returns
	// the list of results which can then be
	// used to render the page
	var results = response.all();
	
	// simulate a click -- 'conversion'
	var clicked = response.first();

	// mark the clicked result as converted
	// by using the `.uuid` property
	pio.convert(clicked.uuid);
}).catch(function (err) {
	console.log('Personalize failed: ' + err);
});

// Converting clicks.
// Each result comes with a guaranteed
// random UUID (Result UUID). This UUID can be sent 
// back to PrimedIO to mark a result as having been
// converted. This in turn allows the system
// to evaluate the performance (as CTR) of the
// underlying Model (blend).
pio.convert({
	ruuid: "df3870dc-b277-11e7-abc4-cec278b6b50a"
}); 
```

## Initialization
```
PRIMED.init({
	api_url: "https://primed.io",
	public_key: "xxxxxxxxxxxxxxxxxxxxxxxx",
	secret_key: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| api_url | string | Yes | sets the Primed.io API url | `"https://primed.io"`` |
| public_key | string | Yes | sets the public key | `"xxxxxxxxxxxxxxxxxxxxxxxx"` |
| secret_key | string | Yes | sets the secret key | `"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"` |

## Personalisation
Personalisation requires at least a campaign key (NB, this is not the campaign name), e.g. `frontpage.recommendations`. 

```
pio.personalize({
	campaign: "frontpage.recommendations",
	signals: { itemId: '2503001'},
	limit: 5,
	abvariant: 'A'
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| campaign | string | Yes | campaign key for which personalisation is retrieved | `frontpage.recommendations` |
| signals | dictionary | No (defaults to `{}`) | key, value pairs of signals (itemId currently being viewed, deviceId, location, etc.) | `{itemId: '1234', userId: 'xyz'}` |
| limit | integer | No (defaults to `5`) | number of desired results | `10` |
| abvariant | string | No (defaults to `WRANDOM` assignment) | specify A/B variant for which to retrieve personalisation | `__CONTROL__` |

## Conversion
Upon successful personalisation, a list of results will be returned. Each result will contain a variable payload: the idea here is that PrimedIO is generic and supports freeform `Targets`, which can be item ID's (usually used in lookups after the personalize call), URL's, Boolean values, and any combination of these. Additionally, each result will contain a unique RUUID (Result UUID), randomly generated for this particular call and `Target`. It is used to track the conversion of this particular outcome, which we use to identify which of the A/B variants performed best. Conversion is also generic, but generally we can associate this with clicking a particular outcome. In order for PrimedIO to register this feedback, another call needs to be made upon conversion. This in turn allows the system to evaluate the performance (as CTR) of the underlying Model (blend).

```
pio.convert({
	ruuid: "6d2e36d1-1b58-4fbc-bea8-868e3ec11c87",
	data: { heartbeat: 0.1 }
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| ruuid | string | Yes | ruuid for which to register conversion | `"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"` |
| data | dictionary | No | freeform data payload | `{ heartbeat: 0.1 }` |