FROM mhart/alpine-node:9.2.0
MAINTAINER Matthijs van der Kroon <matthijs@primed.io>
LABEL Description="PrimedIO PrimedNodeJS test container" Vendor="PrimedIO" Version="2.1.0"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
       bash curl

WORKDIR /usr/src/app
COPY . .

RUN npm install

ENTRYPOINT ["sh"]