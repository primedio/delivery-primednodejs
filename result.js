'use strict';

module.exports = {
	Result: Result,
	ResultSet: ResultSet
}

function Result(
	components, 
	recency_factor,
	ruuid,
	target,
	fscore) {
  
	this._components = components;
	this._recency_factor = recency_factor;
	this._ruuid = ruuid;
	this._target = target;
	this._fscore = fscore;
}

function ResultSet(
	abvariant_label, 
	dithering,
	recency,
	guuid, 
	query_latency_ms, 
	results) {
  
	this.abvariant_label = abvariant_label;
	this.dithering = dithering;
	this.recency = recency;
	this.guuid = guuid;
	this.query_latency_ms = query_latency_ms;
	this._results = results || [];
}

// class methods
Result.prototype.get = function() {
	return this._target._value;
}

Result.prototype.ruuid = function() {
	return this._ruuid;
}

ResultSet.prototype.first = function() {	
	return this._results[0];
}

ResultSet.prototype.all = function() {
	var results = [];
	for (var i = 0, len = this._results.length; i < len; i++) {
		results.push({
			"uuid": this._results[i]._ruuid, 
			"value": this._results[i]._target._value
		});
	}
	return results;
};